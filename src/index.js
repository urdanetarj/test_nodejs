import '@babel/polyfill';

import app from "./server";
import {connect} from "./database";

//app.listen(3000);

async  function  main(){
   await app.listen(app.get('port'));
   connect();
   console.log('Server port:', app.get('port'));
}

main();