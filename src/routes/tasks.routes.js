import {Router} from 'express';
import {connect} from "../database";
const bodyParser = require('body-parser');
const router=Router();


const Users=require('../models/users');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


router.get('/getUser/:username', async (req,res) => {
    const db = await connect();
    const username=req.params.username;
        console.log(req.params);
    Users.find({username:username}, function (err, users) {
        if (err){
            console.log('ERROR: Hubo un error al intentar procesar el request getUser')
            res.send(err);

        }else{
            console.log('INFO: Procesamos request getUser | username:'+username);
            res.json(users);
        }


    });
});

router.post('/desactiveUser', async (req,res) => {
    const db = await connect();
    const username=req.body.username;


    Users.remove({username:username}, function (err, users) {
        if (err){
            console.log('ERROR: Hubo un error al intentar procesar el request desactiveUser')
            res.send(err);
        }else{
            console.log('INFO: Procesamos request desactiveUser | username:'+req.body.username);
            res.send('status_code=0')
        }


    });
});



router.post('/activateUser', async (req,res) => {
        const db = await connect();
        const username=req.body.username;

     Users.find({username:username}, function (err, users) {
        if (err){
            console.log('ERROR: Hubo un error al intentar procesar el request activateUser')
            res.send(err);
        }else{
            console.log('INFO: Procesamos request activateUser | username:'+req.body.username);
            res.send('status_code=0')
           // res.json(users);
        }


    });
});

/**
 *
 */

router.post('/addUser', async (req,res) => {
    const db = await connect();

    const users=new Users({
        username:req.body.username,
        email:req.body.email,
        password:req.body.password
    });

    try{
        const newusers=await users.save();
        console.log('INFO: Procesamos request addUser | username:'+req.body.username+'| email:'+req.body.email+'| password:'+req.body.password);
        res.status(201).json(newusers);
    }catch (e){
        console.log('ERROR: Hubo un error al intentar procesar el request addUser');
        res.status(400).json({message:e.message});
    }
});

export  default router;


