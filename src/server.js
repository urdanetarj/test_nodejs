import express from 'express';
import IndexRoutes from './routes/index.route';
import TaskRoutes from './routes/tasks.routes';
const app=express();

app.set('port', process.env.PORT || 8080 )

app.use(IndexRoutes);
app.use('/users',TaskRoutes);
export default  app;